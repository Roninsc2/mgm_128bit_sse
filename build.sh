#!/bin/bash 
gcc -std=c99 -msse4.2 -msse4.1 -msse3 -msse2 -mpclmul -O2 src/main.c \
	        					  src/mgm_mode.c \
	        				          src/utility.c	\
	                                                  -o mgm_test

