#include "utility.h"
#include "mgm_mode.h"
#include <stdio.h>
#include <stdlib.h>


void FreeWorkingData(TContextMGM128 *ctx, __m128i *key, __m128i *nonce,
                     __m128i *data, __m128i *cryptoData,
                     __m128i *associated, __m128i *imitation)
{
    MGM128_FreeContext(ctx);
    if (key) {
        free(key);
    }
    if (nonce) {
        free(nonce);
    }
    if (data) {
        free(data);
    }
    if (cryptoData) {
        free(cryptoData);
    }
    if (associated) {
        free(associated);
    }
    if (imitation) {
        free(imitation);
    }
}

int ReadKeyData(char *name, __m128i **key) {
    FILE * f = fopen(name, "rb");
    if (f == NULL) {
        printf("can't open key file %s\n", name);
        return 1;
    }
    fseek(f, 0, SEEK_END);
    uint64_t size = ftell(f);
    if (size != 64) {
        printf("incorrect key size : %lu\n", size);
        fclose(f);
        return 1;
    }
    *key = (__m128i*)malloc(sizeof(__m128i)*4);
    fseek(f, 0, SEEK_SET);
    if (size != fread(*key, 1, size, f)) {
        printf("key read failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("key data read success\n");
    return 0;
}

int ReadNonceData(char *name, __m128i **nonce) {
    FILE * f = fopen(name, "rb");
    if (f == NULL) {
        printf("can't open nonce file %s\n", name);
        return 1;
    }
    fseek(f, 0, SEEK_END);
    uint64_t size = ftell(f);
    if (size != 16) {
        printf("incorrect nonce size : %lu\n", size);
        fclose(f);
        return 1;
    }
    *nonce = (__m128i*)malloc(sizeof(__m128i));
    fseek(f, 0, SEEK_SET);
    if (size != fread(*nonce, 1, size, f)) {
        printf("nonce read failure\n");
        fclose(f);
        return 1;
    }
    if (((uint8_t*)(*nonce))[0] & 0x80) {
        printf("nonce data has size 128 bits, should be 127 bits\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("nonce data read success\n");
    return 0;
}

int ReadAssociatedData(char *name, __m128i **associated, uint64_t *associatedSize) {
    FILE * f = fopen(name, "rb");
    if (f == NULL) {
        printf("can't open associated file %s\n", name);
        return 1;
    }
    fseek(f, 0, SEEK_END);
    uint64_t size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *associatedSize = size;
    *associated = (__m128i*)malloc(sizeof(__m128i) * (size/16 + (size % 16 > 0)));
    if (size != fread(*associated, 1, size, f)) {
        printf("associated read failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("associated data read success\n");
    return 0;
}

int ReadTextData(char *name, __m128i **data, uint64_t *dataSize) {
    FILE * f = fopen(name, "rb");
    if (f == NULL) {
        printf("can't open text data file %s\n", name);
        return 1;
    }
    fseek(f, 0, SEEK_END);
    uint64_t size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *dataSize = size;
    *data = (__m128i*)malloc(sizeof(__m128i)*(size/16 + (size % 16 > 0)));
    if (size != fread(*data, 1, size, f)) {
        printf("text read failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("text data read success\n");
    return 0;
}

int ReadCryptoData(char *name, __m128i **data, uint64_t *dataSize) {
    FILE * f = fopen(name, "rb");
    if (f == NULL) {
        printf("can't open crypto data file %s\n", name);
        return 1;
    }
    fseek(f, 0, SEEK_END);
    uint64_t size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *dataSize = size;
    *data = (__m128i*)malloc(sizeof(__m128i) * (size/16 + (size % 16 > 0)));
    if (size != fread(*data, 1, size, f)) {
        printf("crypto read failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("crypto data read success\n");
    return 0;
}

int ReadImitationData(char *name, __m128i **imitation) {
    FILE * f = fopen(name, "rb");
    if (f == NULL) {
        printf("can't open imitation file %s\n", name);
        return 1;
    }
    fseek(f, 0, SEEK_END);
    uint64_t size = ftell(f);
    if (size != 16) {
        printf("incorrect imitation size : %ld\n", size);
        fclose(f);
        return 1;
    }
    fseek(f, 0, SEEK_SET);
    *imitation = (__m128i*)malloc(sizeof(__m128i));
    if (size != fread(*imitation, 1, size, f)) {
        printf("imitation read failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("imitation data read success\n");
    return 0;
}

int WriteTextData(char *name,  __m128i *data, uint64_t dataSize) {
    FILE * f = fopen(name, "wb");
    if (f == NULL) {
        printf("can't open text file %s\n", name);
        return 1;
    }
    if (dataSize != fwrite(data, 1, dataSize, f)) {
        printf("text write failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("text data write success\n");
    return 0;
}

int WriteCryptoData(char *name, __m128i *data, uint64_t dataSize) {
    FILE * f = fopen(name, "wb");
    if (f == NULL) {
        printf("can't open crypto file %s\n", name);
        return 1;
    }
    if (dataSize != fwrite(data, 1, dataSize, f)) {
        printf("crypto write failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("crypto data write success\n");
    return 0;
}

int WriteImitationData(char *name, __m128i *imitation) {
    FILE * f = fopen(name, "wb");
    if (f == NULL) {
        printf("can't open imitation file %s\n", name);
        return 1;
    }
    if (16 != fwrite(imitation, 1, 16, f)) {
        printf("imitation write failure\n");
        fclose(f);
        return 1;
    }
    fclose(f);
    printf("imitation data write success\n");
    return 0;
}


int EncryptMGM128(char * keyFile, char * nonceFile, char * dataFile, char * associatedFile,
            char * cryptoDataFile, char * imitationFile)
{
    TContextMGM128 context;
    MGM128_CreateContext(&context);
    __m128i *key = NULL;
    __m128i *nonce = NULL;
    __m128i *imitation = NULL;
    __m128i *data = NULL;
    __m128i *cryptoData = NULL;
    __m128i *associated = NULL;
    uint64_t dataSize, associatedSize;
    if (ReadKeyData(keyFile, &key) || ReadNonceData(nonceFile, &nonce) ||
            ReadTextData(dataFile, &data, &dataSize) ||
            ReadAssociatedData(associatedFile, &associated, &associatedSize) || MGM128_SetKey(&context, key)
            || MGM128_SetNonce(&context, nonce))
    {
        FreeWorkingData(&context, key, nonce, data, cryptoData, associated, imitation);
        return 1;
    }
    cryptoData = (__m128i*)malloc(sizeof (__m128i) * (dataSize/16 + (dataSize % 16 > 0)));
    imitation = (__m128i*)malloc(sizeof (__m128i));
    MGM128_EncryptData(&context, data, cryptoData, associated, imitation, dataSize, associatedSize);
    WriteCryptoData(cryptoDataFile, cryptoData, dataSize);
    WriteImitationData(imitationFile, imitation);
    FreeWorkingData(&context, key, nonce, data, cryptoData, associated, imitation);
    return 0;
}

int DecryptMGM128(char * keyFile, char * nonceFile, char * dataFile, char * associatedFile,
                  char * cryptoDataFile, char * imitationFile)
{
    TContextMGM128 context;
    MGM128_CreateContext(&context);
    __m128i *key = NULL;
    __m128i *nonce = NULL;
    __m128i *imitation = NULL;
    __m128i *data = NULL;
    __m128i *cryptoData = NULL;
    __m128i *associated = NULL;
    uint64_t dataSize, associatedSize;
    if (ReadKeyData(keyFile, &key) || ReadNonceData(nonceFile, &nonce) || ReadImitationData(imitationFile, &imitation) ||
            ReadCryptoData(cryptoDataFile, &cryptoData, &dataSize) ||
            ReadAssociatedData(associatedFile, &associated, &associatedSize) || MGM128_SetKey(&context, key)
            || MGM128_SetNonce(&context, nonce))
    {
        FreeWorkingData(&context, key, nonce, data, cryptoData, associated, imitation);
        return 1;
    }
    data = (__m128i*)malloc(sizeof (__m128i) * (dataSize/16 + (dataSize % 16 > 0)));
    if (MGM128_DecryptData(&context, data, cryptoData, associated, imitation, dataSize, associatedSize)) {
        printf("imitation calculate failure\n");
        FreeWorkingData(&context, key, nonce, data, cryptoData, associated, imitation);
        return 1;
    }
    WriteTextData(dataFile, data, dataSize);
    FreeWorkingData(&context, key, nonce, data, cryptoData, associated, imitation);
    return 0;
}
