#pragma once


int EncryptMGM128(char * keyFile, char * nonceFile, char * dataFile, char * associatedFile,
            char * cryptoDataFile, char * imitationFile);

int DecryptMGM128(char * keyFile, char * nonceFile, char * dataFile, char * associatedFile,
            char * cryptoDataFile, char * imitationFile);
