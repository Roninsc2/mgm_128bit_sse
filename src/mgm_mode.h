#pragma once

#include <stdint.h>
#include <emmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>

#define mgm128_blockLen 16

typedef __m128i (*encrypt)(__m128i in, __m128i  *roundKeys);
typedef void (*set_key)(__m128i *keyStr, __m128i *roundKeys);


typedef struct {
    __m128i * roundKeys;
    __m128i * key;
    __m128i * nonce;
    encrypt Encrypt;
    set_key SetKey;
} TContextMGM128;

int MGM128_EncryptData(TContextMGM128 *ctx, __m128i *data, __m128i *cryptoData,
                       __m128i* associated, __m128i *imitation, const uint64_t dataSize, const uint64_t associatedSize);
int MGM128_DecryptData(TContextMGM128 *ctx, __m128i *data, __m128i *cryptoData,
                       __m128i* associated, __m128i *imitation, const uint64_t dataSize, const uint64_t associatedSize);
void MGM128_CreateContext(TContextMGM128* ctx);
void MGM128_FreeContext(TContextMGM128 *ctx);
int MGM128_SetKey(TContextMGM128 *ctx, const __m128i *key);
int MGM128_SetNonce(TContextMGM128 *ctx, const __m128i *nonce);
