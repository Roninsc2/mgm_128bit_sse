#include <stdio.h>
#include <stdlib.h>
#include "utility.h"

enum EMode {
    Encrypt,
    Decrypt
};

//working/key.bin working/nonce.bin working/data.bin working/associated.bin working/cryptoData.bin working/imitation.bin
//[0-1] <key file> <nonce file> <data file> <associated file> <cryptoData file> <imitation file>
int main(int argc, char** argv) {
    char help[] ="[0-1] <key file> <nonce file> <data file> <associated file> <cryptoData file> <imitation file>"
                 "\n0 - Encyrpt\n1 - Decrypt";
    if (argc != 8 || argv[1][0] > '1' || argv[1][0] < '0') {
        printf("%s", help);
        return 1;
    }
    enum EMode mode = argv[1][0] - '0';
    char * keyFile = argv[2];
    char * nonceFile = argv[3];
    char * dataFile = argv[4];
    char * associatedFile = argv[5];
    char * cryptoDataFile = argv[6];
    char * imitationFile = argv[7];

    switch (mode) {
    case Encrypt: {
        EncryptMGM128(keyFile, nonceFile, dataFile, associatedFile, cryptoDataFile, imitationFile);
        break;
    }
    case Decrypt: {
        DecryptMGM128(keyFile, nonceFile, dataFile, associatedFile, cryptoDataFile, imitationFile);
        break;
    }
    default:
        break;
    }
    return 0;
}
