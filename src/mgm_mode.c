#include "mgm_mode.h"
#include "tables.h"
#include <string.h>

//kuznechick_start

static inline __m128i FuncLS(__m128i val) {
    __m128i temp0;
    __m128i temp1;
    __m128i addr0;
    __m128i addr1;

    addr0 = _mm_and_si128(*((const __m128i *)bitmask), val);
    addr1 = _mm_andnot_si128(*((const __m128i *)bitmask), val);

    addr0 = _mm_srli_epi16(addr0, 4);
    addr1 = _mm_slli_epi16(addr1, 4);

    temp0 = _mm_load_si128((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 0) + 0x1000));
    temp1 = _mm_load_si128((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 0) + 0x0000));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS+ _mm_extract_epi16(addr0, 1)+ 0x3000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 1) + 0x2000)));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 2) + 0x5000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 2) + 0x4000)));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 3) + 0x7000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 3) + 0x6000)));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 4) + 0x9000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 4) + 0x8000)));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 5) + 0xB000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 5) + 0xA000)));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 6) + 0xD000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 6) + 0xC000)));

    temp0 = _mm_xor_si128(temp0, *((const __m128i *) (tableLS + _mm_extract_epi16(addr0, 7) + 0xF000)));
    temp1 = _mm_xor_si128(temp1, *((const __m128i *) (tableLS + _mm_extract_epi16(addr1, 7) + 0xE000)));

    return _mm_xor_si128(temp0, temp1);
}

static inline void FuncF(const uint8_t *k, __m128i *a1, __m128i *a0) { //  ---> (f(a1) + a0, a1)
    __m128i temp0;
    __m128i temp1 = _mm_loadu_si128((const __m128i *)k);
    temp0 = *a1;
    *a1 = _mm_xor_si128(temp1, *a1); //func X
    *a1 = FuncLS(*a1);
    *a1 = _mm_xor_si128(*a0, *a1);
    *a0 = temp0;
}

static inline void SetKeyKuznechick(__m128i *key, __m128i *roundKeys) {
    roundKeys[0] = key[0];
    roundKeys[1] = key[1];
    __m128i key0;
    __m128i key1;
    for (uint8_t i = 0; i < 4; i++) {
        key0 = roundKeys[2*i];
        key1 = roundKeys[2*i + 1];
        FuncF(&iterationConst[128*i + 0], &key0, &key1);
        FuncF(&iterationConst[128*i + 16], &key0, &key1);
        FuncF(&iterationConst[128*i + 32], &key0, &key1);
        FuncF(&iterationConst[128*i + 48], &key0, &key1);
        FuncF(&iterationConst[128*i + 64], &key0, &key1);
        FuncF(&iterationConst[128*i + 80], &key0, &key1);
        FuncF(&iterationConst[128*i + 96], &key0, &key1);
        FuncF(&iterationConst[128*i + 112], &key0, &key1);
        roundKeys[2*(i+1)] = key0;
        roundKeys[2*(i+1) + 1] =  key1;
    }
}

static inline __m128i EncryptKuznechik(__m128i in, __m128i  *roundKeys) {
    in = _mm_xor_si128(roundKeys[0], in); //func X
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[1], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[2], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[3], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[4], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[5], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[6], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[7], in);
    in = FuncLS(in);
    in = _mm_xor_si128(roundKeys[8], in);
    in = FuncLS(in);
    return _mm_xor_si128(roundKeys[9], in);
}

//kuznechick_end

// multiplication mod f(x) = x^128 + x^7 + x^2 + x + 1
static inline __m128i MGMMulGF128(__m128i a, __m128i b) {
    __m128i tmp0, tmp1, tmp2, tmp3, tmp4,
            tmp5, tmp6, tmp7, tmp8, tmp9;
    __m128i XMM_MASK = _mm_setr_epi32(0xFFFFFFFF, 0x0, 0x0, 0x0);
    __m128i BSWAP_MASK = _mm_set_epi8(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
    a = _mm_shuffle_epi8(a, BSWAP_MASK);
    b = _mm_shuffle_epi8(b, BSWAP_MASK);
    tmp0 = _mm_clmulepi64_si128(a, b, 0x00);
    tmp3 = _mm_clmulepi64_si128(a, b, 0x11);
    tmp1 = _mm_shuffle_epi32(a,78);
    tmp2 = _mm_shuffle_epi32(b,78);
    tmp1 = _mm_xor_si128(tmp1, a);
    tmp2 = _mm_xor_si128(tmp2, b);
    tmp1 = _mm_clmulepi64_si128(tmp1, tmp2, 0x00);
    tmp1 = _mm_xor_si128(tmp1, tmp0);
    tmp1 = _mm_xor_si128(tmp1, tmp3);
    tmp2 = _mm_slli_si128(tmp1, 8);
    tmp1 = _mm_srli_si128(tmp1, 8);
    tmp0 = _mm_xor_si128(tmp0, tmp2);
    tmp3 = _mm_xor_si128(tmp3, tmp1);
    tmp4 = _mm_srli_epi32(tmp3, 31);
    tmp5 = _mm_srli_epi32(tmp3, 30);
    tmp6 = _mm_srli_epi32(tmp3, 25);
    tmp4 = _mm_xor_si128(tmp4, tmp5);
    tmp4 = _mm_xor_si128(tmp4, tmp6);
    tmp5 = _mm_shuffle_epi32(tmp4, 147);
    tmp4 = _mm_and_si128(XMM_MASK, tmp5);
    tmp5 = _mm_andnot_si128(XMM_MASK, tmp5);
    tmp0 = _mm_xor_si128(tmp0, tmp5);
    tmp3 = _mm_xor_si128(tmp3, tmp4);
    tmp7 = _mm_slli_epi32(tmp3, 1);
    tmp0 = _mm_xor_si128(tmp0, tmp7);
    tmp8 = _mm_slli_epi32(tmp3, 2);
    tmp0 = _mm_xor_si128(tmp0, tmp8);
    tmp9 = _mm_slli_epi32(tmp3, 7);
    tmp0 = _mm_xor_si128(tmp0, tmp9);

    return _mm_xor_si128(tmp0, tmp3);
}

static inline void IncrR(__m128i *val) {
    uint64_t incVal = ((uint64_t*)val)[1];
    incVal = __builtin_bswap64(incVal) + 1;
    ((uint64_t*)val)[1] = __builtin_bswap64(incVal);
}

static inline void IncrL(__m128i *val) {
    uint64_t incVal = ((uint64_t*)val)[0];
    incVal = __builtin_bswap64(incVal) + 1;
    ((uint64_t*)val)[0] = __builtin_bswap64(incVal);
}


//-----Imitation Calculate---------
//SUM(a, b) MULT(a, b) in GF(2^128)
//SUM(MULT(H_i, A_i), ...) , i from 1 to h; <--- first part, varible 'a'
//SUM(MULT(H_(h+j), C_j), ...) , j from 1 to q; <--- second part, varible 'b'
//MULT(H_(h+q+1), (l(A)||l(C)); <--- third part, varible 'c'
void ImitationEncrypt(TContextMGM128 *context, __m128i *cryptoData, __m128i* associated,
                      __m128i *imitation, const uint64_t dataSize,
                      const uint64_t associatedSize)
{
    //prepare data
    uint8_t associatedRemainder = associatedSize % mgm128_blockLen;
    uint8_t dataRemainder = dataSize % mgm128_blockLen;
    uint64_t associated128Size = associatedSize/mgm128_blockLen + (associatedSize % mgm128_blockLen > 0);
    uint64_t data128Size = dataSize/mgm128_blockLen + (dataSize % mgm128_blockLen > 0);
    if (associatedRemainder) {
        __m128i *lastBlock = &(associated[associated128Size-1]);
        for (uint8_t i = associatedRemainder; i < mgm128_blockLen; i++) {
            ((uint8_t*)lastBlock)[i] = 0x00;
        }
    }

    if (dataRemainder) {
        __m128i *lastBlock = &(cryptoData[data128Size-1]);
        for (uint8_t i = dataRemainder; i < mgm128_blockLen; i++) {
            ((uint8_t*)lastBlock)[i] = 0x00;
        }
    }
    //end prepare data

    //init hepl values
    __m128i z, h, block, x, acc;
    acc = _mm_set_epi64x(0, 0);

    z = context->nonce[0];
    ((uint8_t *)&z)[0] = ((uint8_t *)&z)[0] | 0x80; // == 1 || N
    z = context->Encrypt(z, context->roundKeys);

    //calculate first part; block == A_i
    for (uint64_t i = 0; i < associated128Size; i++) {
        block = associated[i];
        h = context->Encrypt(z, context->roundKeys);
        x = MGMMulGF128(h, block);
        acc = _mm_xor_si128(acc, x);
        IncrL(&z);
    }

    //calculate second part; block == C_i
    for (uint64_t i = 0; i < data128Size; i++) {
        block = cryptoData[i];
        h = context->Encrypt(z, context->roundKeys);
        x = MGMMulGF128(h, block);
        acc = _mm_xor_si128(acc, x);
        IncrL(&z);
    }

    //calculate third part; block == (l(A)||l(C)), where l(x) ---> size of x in bits
    h = context->Encrypt(z, context->roundKeys);
    block = _mm_insert_epi64(block, associatedSize * 8, 1);
    block = _mm_insert_epi64(block, dataSize * 8, 0);
    __m128i BSWAP_MASK = _mm_set_epi8(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
    block = _mm_shuffle_epi8(block, BSWAP_MASK);
    x = MGMMulGF128(h, block);
    acc = _mm_xor_si128(acc, x);
    acc = _mm_shuffle_epi8(acc, BSWAP_MASK);
    //calculate imitation
    *imitation = context->Encrypt(acc, context->roundKeys);
}

int ImitationDecrypt(TContextMGM128 *context, __m128i *cryptoData, __m128i* associated,
                     const __m128i *imitation, const uint64_t dataSize,
                     const uint64_t associatedSize)
{
    //prepare data
    uint8_t associatedRemainder = associatedSize % mgm128_blockLen;
    uint8_t dataRemainder = dataSize % mgm128_blockLen;
    uint64_t associated128Size = associatedSize/mgm128_blockLen + (associatedSize % mgm128_blockLen > 0);
    uint64_t data128Size = dataSize/mgm128_blockLen + (dataSize % mgm128_blockLen > 0);
    if (associatedRemainder) {
        __m128i *lastBlock = &(associated[associated128Size-1]);
        for (uint8_t i = associatedRemainder; i < mgm128_blockLen; i++) {
            ((uint8_t*)lastBlock)[i] = 0x00;
        }
    }

    if (dataRemainder) {
        __m128i *lastBlock = &(cryptoData[data128Size-1]);
        for (uint8_t i = dataRemainder; i < mgm128_blockLen; i++) {
            ((uint8_t*)lastBlock)[i] = 0x00;
        }
    }
    //end prepare data

    //init hepl values
    __m128i z, h, block, x, acc;
    acc = _mm_set_epi64x(0, 0);

    z = context->nonce[0];
    ((uint8_t *)&z)[0] = ((uint8_t *)&z)[0] | 0x80; // == 1 || N
    z = context->Encrypt(z, context->roundKeys);

    //calculate first part; block == A_i
    for (uint64_t i = 0; i < associated128Size; i++) {
        block = associated[i];
        h = context->Encrypt(z, context->roundKeys);
        x = MGMMulGF128(h, block);
        acc = _mm_xor_si128(acc, x);
        IncrL(&z);
    }

    //calculate second part; block == C_i
    for (uint64_t i = 0; i < data128Size; i++) {
        block = cryptoData[i];
        h = context->Encrypt(z, context->roundKeys);
        x = MGMMulGF128(h, block);
        acc = _mm_xor_si128(acc, x);
        IncrL(&z);
    }

    //calculate third part; block == (l(A)||l(C)), where l(x) ---> size of x in bits
    h = context->Encrypt(z, context->roundKeys);
    block = _mm_insert_epi64(block, associatedSize * 8, 1);
    block = _mm_insert_epi64(block, dataSize * 8, 0);
    __m128i BSWAP_MASK = _mm_set_epi8(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
    block = _mm_shuffle_epi8(block, BSWAP_MASK);
    x = MGMMulGF128(h, block);
    acc = _mm_xor_si128(acc, x);
    acc = _mm_shuffle_epi8(acc, BSWAP_MASK);

    //calculate temp imitation
    x = context->Encrypt(acc, context->roundKeys); // x is temp imitation
    z = _mm_xor_si128(x, *imitation);
    return !_mm_test_all_zeros(z, z);
}


void DoEncrypt(TContextMGM128 *context, const __m128i *data,
               __m128i *cryptoData, const uint64_t dataSize)
{
    uint64_t data128Size = dataSize/mgm128_blockLen + (dataSize % mgm128_blockLen > 0);
    __m128i y, encryptY;
    y = context->Encrypt(*(context->nonce), context->roundKeys);
    //calculate crypto blocks C_i from 1 to q-1, size of C_i = n bytes
    for (uint64_t i = 0; i < data128Size; i++) {
        encryptY = context->Encrypt(y, context->roundKeys);
        cryptoData[i] = _mm_xor_si128(data[i], encryptY);
        IncrR(&y); //Y_(i-1) --->Y_i
    }
}


void DoDecrypt(TContextMGM128 *context, __m128i *data,
               const __m128i *cryptoData, uint64_t dataSize)
{
    uint64_t data128Size = dataSize/mgm128_blockLen + (dataSize % mgm128_blockLen > 0);
    __m128i y, encryptY;
    y = context->Encrypt(*(context->nonce), context->roundKeys);
    //calculate crypto blocks C_i from 1 to q-1, size of C_i = 16 bytes
    for (uint64_t i = 0; i < data128Size; i++) {
        encryptY = context->Encrypt(y, context->roundKeys);
        data[i] = _mm_xor_si128(cryptoData[i], encryptY);
        IncrR(&y); //Y_(i-1) --->Y_i
    }
}


int MGM128_DecryptData(TContextMGM128 *ctx, __m128i *data,
                       __m128i *cryptoData, __m128i* associated,
                       __m128i *imitation, const uint64_t dataSize,
                       const uint64_t associatedSize)
{
    if (!ctx || !ctx->key || !ctx->nonce || !data ||
            !cryptoData || !associated || !imitation)
    {
        return 1;
    }
    ctx->SetKey(&ctx->key[2], ctx->roundKeys);
    if (ImitationDecrypt(ctx, cryptoData, associated,
                         imitation, dataSize, associatedSize))
    {
        return 1;
    }
    ctx->SetKey(&ctx->key[0], ctx->roundKeys);
    DoDecrypt(ctx, data, cryptoData, dataSize);
    return 0;
}

int MGM128_EncryptData(TContextMGM128 *ctx, __m128i *data,
                       __m128i *cryptoData, __m128i* associated,
                       __m128i *imitation, const uint64_t dataSize,
                       const uint64_t associatedSize)
{
    if (!ctx || !ctx->key || !ctx->nonce || !data ||
            !cryptoData || !associated || !imitation)
    {
        return 1;
    }
    ctx->SetKey(&ctx->key[0], ctx->roundKeys);
    DoEncrypt(ctx, data, cryptoData, dataSize);
    ctx->SetKey(&ctx->key[2], ctx->roundKeys);
    ImitationEncrypt(ctx, cryptoData, associated, imitation, dataSize, associatedSize);
    return 0;
}

void MGM128_CreateContext(TContextMGM128* context) {
    context->key = (__m128i*)malloc(sizeof(__m128i) * 4);;
    context->roundKeys = (__m128i *)malloc(sizeof(__m128i)*10);
    context->nonce = (__m128i*)malloc(sizeof(__m128i));
    context->Encrypt = EncryptKuznechik;
    context->SetKey = SetKeyKuznechick;
}

void MGM128_FreeContext(TContextMGM128 *context) {
    if (context->key) {
        free(context->key);
    }
    if (context->nonce) {
        free(context->nonce);
    }
    if (context->roundKeys) {
        free(context->roundKeys);
    }
}

int MGM128_SetKey(TContextMGM128 *ctx, const __m128i *key) {
    if (!ctx || !key) {
        return 1;
    }
    memcpy(ctx->key, key, 4*mgm128_blockLen);
    return 0;
}

int MGM128_SetNonce(TContextMGM128 *ctx, const __m128i *nonce) {
    if (!ctx || !nonce) {
        return  1;
    }
    memcpy(ctx->nonce, nonce, mgm128_blockLen);
    return 0;
}
